package main
import "C"
//Necessary Package Imports
import (
        "bytes"
        "fmt"
        "image"
        "image/png"
        "io"
        "io/ioutil"
        "log"
        "math"
        "net/http"
        "reflect"
        "strconv"
        "strings"
        "time"
        "os"


)

//Block Image URL
//var webpath = "https://dev.vrate.net/images/block.svg"
/*
  Function Name: skintest
  Function Description: 
*/

func skintest(w http.ResponseWriter, r *http.Request) {
        //Handle all http POST reqeusts here
        //Enable CORS
        w.Header().Set("Access-Control-Allow-Origin", "*")
        w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        w.Header().Set("Access-Control-Allow-Headers",
                "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
        start := time.Now()
        log.Println(start, "start time for getting image")
        url_pr := r.URL.String()
        log.Println(url_pr, "url got from request")
        racy := url_pr[strings.Index(url_pr, "?")+1 : strings.Index(url_pr, "?")+2]
        fmt.Println(racy, "Racy content")
        uri  := url_pr[strings.Index(url_pr, "?")+2 : len(url_pr)]
        fmt.Println(uri, "new url from request")
        rep1 := strings.Replace(uri, "%3A", ":", -1)
        urls := strings.Replace(rep1, "%2F", "/", -1)
        log.Println(urls, "Modified url")
        fmt.Println("I m in")
        processURL(string(urls), http.ResponseWriter(w),racy,r)
        end := time.Now()
        log.Println(end.Sub(start), "--Total time taken to process the image")

}

/*
  Function Name: showImage
  Function Description: 
*/
func showImage(class string) bool {
        switch class {
        case "02":
                return false
        case "03":
                return true
        case "00":
                return true
        case "01":
                return true
        default:
                return true

        }


}

/*
  Function Name: blockDecision
  Function Description: 
*/
func blockDecision(show bool, racy string, imgClass string, w http.ResponseWriter) bool {
	var block = false
	if (show) && (racy == "1") && (imgClass == "01") {
		log.Println("Process block process page as the racy flag and the label are 01 ")
		block = true
	} else if (show) && (racy == "1") && (imgClass != "01") {
		log.Println("Showing content if race and label are 1 and not 01 respectively ")
		block = false
	} else if (show) && (racy == "0") && (imgClass == "01") {
		log.Println("Showing content if race and label are 0 and 01 respectively ")
		block = false
	} else if (show) && (racy == "0") && (imgClass != "01") {
		log.Println("Showing content for base condition")
		block = false
	} else{
               log.Println("Process block process page - Default case ")
		block = true

        }
	return block
}

/*
  Function Name: processBlockPage
  Function Description: 
*/

func processBlockPage(w http.ResponseWriter) {
        //Enable CORS
        w.Header().Set("Access-Control-Allow-Origin", "*")
        w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
        w.Header().Set("Access-Control-Allow-Headers",
                "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, location")
        resps, err := http.Get("http://dev.vrate.net/block.svg")
        if err != nil {
                log.Println(err, "unable to write image.")
        }
        for k, v := range resps.Header {
                for _, vv := range v {
                        w.Header().Add(k, vv)
                }
        }

        b, err := ioutil.ReadAll(resps.Body)
        if err != nil {
                return
        }
        c := b
        bufs := new(bytes.Buffer)
        bufs.Write(c)
        w.Header().Set("Content-Length", strconv.Itoa(len(bufs.Bytes())))
        w.Header().Set("Access-Control-Expose-Headers", "x-location")
        w.WriteHeader(307)
        if _, err := w.Write(bufs.Bytes()); err != nil {
                log.Println(err, "unable to write image.")
        }

}
/*
  Function Name: writeResp
  Function Description: 
*/

func writeResp(blockDec bool, w http.ResponseWriter, bufs *bytes.Buffer){

             if blockDec{
                        processBlockPage(w)
                        }else{
                              if _, err := w.Write(bufs.Bytes()); err != nil {
                                log.Println(err, "unable to write image.")
                        }

                        }


}

func redirectURL(w http.ResponseWriter,req *http.Request,url string)http.ResponseWriter{
        log.Println("Redirect urls that cannot be processed")
        log.Println(w,"response")
        mod_url :="https://temp.vrate.net?"+url
        w.Header().Set("location", mod_url)
        w.Header().Add("x-location",mod_url)
        w.Header().Set("Access-Control-Expose-Headers", "x-location")
        w.WriteHeader(307)
        log.Println("W headers")
        return w
}

/*
  Function Name: processURL
  Function Description: 
*/

func processURL(url string, w http.ResponseWriter,racy string, req *http.Request) http.ResponseWriter {
          //Enable CORS
        w.Header().Set("Access-Control-Allow-Origin", "*")
        w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
        w.Header().Set("Access-Control-Allow-Headers",
                "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Location")

        var sk_pct = 0.0
        var show = false
        var processed = false
        resp, err := http.Get(url)
        if err != nil {
                log.Println("Got Error")
                w = redirectURL(w,req,url)
                log.Println(err, "unable to get the response.")
                return w
                os.Exit(0)
        }
        if err == nil{
        defer resp.Body.Close()
        }
        contenttype := resp.Header.Get("Content-Type")
        if strings.Contains(contenttype, "png") {
                log.Println("PNG processing side")
                sk_pct = processPNG(io.ReadCloser(resp.Body))
                processed = true
        } else if (strings.Contains(contenttype, "jpg")) || (strings.Contains(contenttype, "jpeg")) {
                log.Println("JPG processing side")
                sk_pct = processJPG(io.ReadCloser(resp.Body))
                processed = true
        } 
        log.Println("Skin percent is %f for url %s/n/n", sk_pct, url)
        log.Println(processed, "<---check if the image is processsed")
        if !processed {
                sk_pct = -0.999
        }
        for k, v := range resp.Header {
                if k == "Content-Security-Policy"{
                   delete(resp.Header,k)
                }
                for _, vv := range v {

                        w.Header().Add(k, vv)
                }
        }
        log.Println(reflect.TypeOf(w.Header()),"Response Type")
        log.Println(w.Header(),"Response headers")
        if sk_pct > 0.0 {
                if sk_pct > 0.05 {
                        fmt.Println("First If condition")

                        resps, err := http.Get(url)
                        if err != nil {
                                http.Redirect(w,req,url,307)
                                return nil
                        }
                        b, err := ioutil.ReadAll(resps.Body)
                        if err != nil {
                                return nil
                        }
                        c := b
                        bufs := new(bytes.Buffer)
                        bufs.Write(c)
                        imgClass := classifyImage(b)
                        log.Println(imgClass, "<----class of the image")
                        show = showImage(imgClass)
                        block:= blockDecision(show,racy,imgClass,w)
                        log.Println(block,"<------Block Decision")
                        writeResp(block,w,bufs)
                      

                } else {
                         var buf bytes.Buffer
                         var e error
                        resps, err := http.Get(url)
                        if err != nil {
                                http.Redirect(w,req,url,307)
                                return nil
                        }
                        b, err := ioutil.ReadAll(resps.Body)
                        if err != nil {
                                return nil
                        }
                        c := b
                        bufs := new(bytes.Buffer)
                        bufs.Write(c)
                        imgtype := extractImgType(b)
                if imgtype == "gif" {
                r := bytes.NewReader(b)
                buf, e = getGifFrame(r)
                if e != nil {
                        log.Println("GIF frame could not be extracted from image")
                        log.Println(e)
                        http.Error(w, "GIF frame could not be extracted from image", http.StatusBadRequest)
                        return nil
                }
                show = true
                imgClass := classifyImage(buf.Bytes())
                fmt.Println(imgClass,"Image Class for GIF")
                block:= blockDecision(show,racy,imgClass,w)
                log.Println(block,"<------Block Decision")
                writeResp(block, w, bufs)

               }else if imgtype == "webp" {
                /*newImage, err := bimg.NewImage(bufs.Bytes()).Convert(bimg.PNG)
		if err != nil {
			log.Println("webP conversion error, actual error below")
			log.Println(err)
			http.Error(w, "Webp image conversion error", http.StatusBadRequest)
			return nil 
		}
                show = true
                imgClass := classifyImage(newImage)             
                block:= blockDecision(show,racy,imgClass,w)
                log.Println(block,"<------Block Decision")
                writeResp(block, w, bufs)*/
               }
                        imgClass := classifyImage(b)
                        fmt.Println(imgClass,"Image Class for jpg/other")
                        show = showImage(imgClass)
                        block:= blockDecision(show,racy,imgClass,w)
                        log.Println(block,"<------Block Decision")
                        writeResp(block, w, bufs)


                }
        } else {
                if strings.Contains(contenttype, "img") {
                log.Println("Image could not processed. So sending the image directly to GPU")
                var buf bytes.Buffer
                var e error
                resps, err := http.Get(url)
                if err != nil {
                        return nil
                }
                b, err := ioutil.ReadAll(resps.Body)
                if err != nil {
                        return nil
                }
                c := b
                bufs := new(bytes.Buffer)
                bufs.Write(c)
                imgtype := extractImgType(b)
                if imgtype == "gif" {
                r := bytes.NewReader(b)
                buf, e = getGifFrame(r)
                if e != nil {
                        log.Println("GIF frame could not be extracted from image")
                        log.Println(e)
                        http.Error(w, "GIF frame could not be extracted from image", http.StatusBadRequest)
                        return nil
                }
                show = true
                imgClass := classifyImage(buf.Bytes())
                log.Println(imgClass,"<-----Image Class for GIF")
                block:= blockDecision(show,racy,imgClass,w)
                writeResp(block, w, bufs)

               }else if imgtype == "webp" {
                /*newImage, err := bimg.NewImage(bufs.Bytes()).Convert(bimg.PNG)
		if err != nil {
			log.Println("webP conversion error, actual error below")
			log.Println(err)
			http.Error(w, "Webp image conversion error", http.StatusBadRequest)
			return nil
		}
                show = true
                fmt.Println("Image Class for webp")
                imgClass := classifyImage(newImage)
                fmt.Println(reflect.TypeOf(newImage),"Type of newImage")
                fmt.Println(imgClass,"Image Class for webp")
                block:= blockDecision(show,racy,imgClass,w)
                fmt.Println(block,"Block")
                writeResp(block, w, bufs)*/
               }

                if _, err := w.Write(bufs.Bytes()); err != nil {
                        log.Println(err, "unable to write image.")
                }
           }else{
                 log.Println("Content is not a image Type")




         }

        }

        //endimg := time.Now()
        log.Println(url, "Done processing")
        return w

}

/*
  Function Name: processPNG
  Function Description: 
*/


func processPNG(body io.ReadCloser) float64 {
        var sk_pct = 0.0
        img, err := png.Decode(body)
        if err != nil {
                return -0.999
        }
        sk_pct = processImage(image.Image(img)) 
        return sk_pct
}

/*
  Function Name: processJPG
  Function Description: 
*/
func processJPG(body io.ReadCloser) float64 {
        var sk_pct = 0.0
        img, _, err := image.Decode(body)
        if err != nil {
                return -0.999
        }
        sk_pct = processImage(image.Image(img))
        return sk_pct
}

/*
  Function Name: processImage
  Function Description: 
*/

func processImage(img image.Image) float64 {
        var sk = 0.0
        g := img.Bounds()
        height := g.Dy()
        width := g.Dx()
        if width > 64 {
                sk = findSkinpct(image.Image(img), int(width), int(height))
        } else {
                sk = -0.999
        }
        return sk
}

/*
  Function Name: findSkinpct
  Function Description: 
*/
func findSkinpct(img image.Image, width int, height int) float64 {
        var skin = 0
        var gray_scale = 0
        var bl_wh = 0
        resolution := width * height
        if resolution < 500000 {
                for y := 0; y < height; y++ {
                        for x := 0; x < width; x++ {
                                r, g, b, a := img.At(x, y).RGBA()
                                var ret = false
                                ret = skin_test(float64(r), float64(g), float64(b), float64(a))
                                if ret {
                                        skin = skin + 1
                                }
                                if (g == r+1) && (b == r+2) {
                                        gray_scale = gray_scale + 1
                                }
                                if (g == r) && (b == g) {
                                        bl_wh = bl_wh + 1
                                }
                        }
                }
        } else {
                skin = resolution
        }
        if float64(gray_scale)/float64(resolution) > 0.05 {
                skin = resolution
        }
        if float64(bl_wh)/float64(resolution) > 0.8 {
                skin = resolution
        }

        skin_pct := float64(skin) / float64(resolution)
        return skin_pct

}

/*
  Function Name: skin_test
  Function Description: 
*/

func skin_test(r, g, b, a float64) bool {
        var alpha = 3 * b * math.Pow(r, float64(2)) / math.Pow((r+b+g), float64(3))
        var beta = (r+g+b)/(3*r) + (r-g)/(r+g+b)
        var gamma = (r*b + math.Pow(g, float64(2))) / (g * b)
        var skin = ((alpha > 0.1276) && (beta <= 0.9498) && (gamma <= 2.7775))
        return skin
}

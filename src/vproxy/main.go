package main

// #cgo pkg-config: opencv cudart-7.5
// #cgo LDFLAGS: -Lcaffe/lib -lcaffe -lglog -lboost_system -lboost_thread
// #cgo CXXFLAGS: -std=c++11 -Icaffe/include -I.. -O2 -fomit-frame-pointer -Wall
// #include <stdlib.h>
// #include "classification.h"
//import "C"
import (
	"bufio"
	"bytes"
	//"encoding/base64"
	"fmt"
	"image/gif"
	"image/jpeg"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
	//"unsafe"
)

//global classifier
//var ctx *C.classifier_ctx

//global dictionary of facklists
var blackList map[string]string

//global cache for http inputs
//var cache, _ = bigcache.NewBigCache(bigcache.DefaultConfig(10080 * time.Minute))

//global access and application log
var url_file_handler *os.File 

//Writelog creates an access log record in the combined log format & writes to the access log file
type statusWriter struct {
	http.ResponseWriter
	status int
	length int
}

func writeLog(handle http.Handler, fileHandler *os.File) http.HandlerFunc {
	logger := log.New(fileHandler, "", 0)
	return func(w http.ResponseWriter, request *http.Request) {
		start := time.Now()
		writer := statusWriter{w, 0, 0}
		handle.ServeHTTP(&writer, request)
		end := time.Now()
		latency := end.Sub(start)
		statusCode := writer.status
		length := writer.length
		if request.URL.RawQuery != "" {
			logger.Printf("%s %s %s %v \"%s %s%s%s %s\" %d %d \"%s\" %v", request.Header.Get("X-Forwarded-For"), "-", "-", end.Format("2006/01/02 15:04:05"), request.Method, request.URL.Path, "?", request.URL.RawQuery, request.Proto, statusCode, length, request.Header.Get("User-Agent"), latency)
		} else {
			logger.Printf("%s %s %s %v \"%s %s %s\" %d %d \"%s\" %v", request.Header.Get("X-Forwarded-For"), "-", "-", end.Format("2006/01/02 15:04:05"), request.Method, request.URL.Path, request.Proto, statusCode, length, request.Header.Get("User-Agent"), latency)
		}
	}
}

//hash function to setup the cache, currently just passthrough
func hash(s string) string {
	return s
}

//getGifFrame decodes an input GIF and returns the first frame
func getGifFrame(gifImage io.Reader) (jpgFrame bytes.Buffer, err error) {

	//Decode gif
	g, err := gif.DecodeAll(gifImage)

	if err != nil {
		return jpgFrame, err
	}

	// return first frame
	sink := bufio.NewWriter(&jpgFrame)
	err = jpeg.Encode(sink, g.Image[0], nil)
	if err != nil {
		return jpgFrame, err
	}
	sink.Flush()

	return jpgFrame, nil

}

//extractImgType will extract and return the MIME type for further processing
func extractImgType(content []byte) string {

	filetype := http.DetectContentType(content)

	switch filetype {
	case "image/jpeg", "image/jpg", "image/png", "image/bmp":
		return "img"
	case "image/gif":
		return "gif"
	case "image/webp":
		//caffe-opencv not handling webp correctly, hence convert to png
		return "webp"
	default:
		fmt.Println(filetype)
		return filetype  //no further processing required
	}

}

//readDict loads the blacklist file into a in-memory go map for fast lookups
func readDict(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	blackList = map[string]string{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		result := strings.Fields(scanner.Text())
		blackList[result[0]] = result[1]
	}
	return scanner.Err()

}


//dlookup handles all the domain lookup work and returns 0 is a website is not in the blacklist
func dlookup(w http.ResponseWriter, r *http.Request) {
	//Handle all http POST reqeusts here
       
	//Enable CORS
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	//reject if key not present in header
	key := r.Header.Get("vrate")
	if key != "4AEB78C497BDC1141E2EE76C35249" {
		http.Error(w, "Invalid key", http.StatusUnauthorized)
		log.Println("Invalid authorization")
		return
	}

	//handle options call
	if r.Method == "OPTIONS" {
		w.WriteHeader(200)
		return
	}

	//reject all other verbs here
	if r.Method != "POST" {
		http.Error(w, "", http.StatusMethodNotAllowed)
		return
	}

	//handle the POST lookup here
	payload, e := ioutil.ReadAll(r.Body)
	if e != nil {
		log.Println("Unable to load POST input data: actual error below..")
		log.Println(e)
		http.Error(w, "Invalid input POST data", http.StatusBadRequest)
		return
	}
       
	//Lookup the go-map global variable & serve the http response
        //fmt.Println(blackList,"BlackListed urls")
	category, foundBool := blackList[string(payload)]
	fmt.Println(blackList[string(payload)])
	if foundBool {
		io.WriteString(w, string(category))
	} else {
		io.WriteString(w, "-1")
	}

}

func classifyImage(bufs []byte) string {
        
     /*var clf_resp=""
	cstr, e := C.classifier_classify(ctx, (*C.char)(unsafe.Pointer(&bufs[0])), C.size_t(len(bufs)))
	if e != nil {
		log.Println("Network prediction generated error or invalid image content, actual error below...")
		log.Println(e)
		//http.Error(w, "Network predict error or invalid image content", http.StatusBadRequest)
		
	}
      
	defer C.free(unsafe.Pointer(cstr))
        fmt.Println("I m in classify")
        clf_resp = C.GoString(cstr)
        cls_temp := clf_resp[strings.Index(clf_resp, ":")+1 : strings.Index(clf_resp, "}")+1]
        cls:= cls_temp[strings.Index(cls_temp, ":")+2 : strings.Index(cls_temp, ":")+4]
	return cls*/
        return "1"
}

//classify is the main function that handles the network prediction
func classify(w http.ResponseWriter, r *http.Request) {
	//Handle all http POST reqeusts here

	//log requests
        /*
	logger := log.New(url_file_handler, "", 0)
	start := time.Now()

	//Enable CORS
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers",
		"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	//handle authorization
	key := r.Header.Get("vrate")
	if key != "4AEB78C497BDC1141E2EE76C35249" {
		http.Error(w, "Invalid key", http.StatusUnauthorized)
		log.Println("Invalid authorization")
		return
	}

	//initialize image buffer
	var buffer []byte

	//handle options call
	if r.Method == "OPTIONS" {
		w.WriteHeader(200)
		return
	}

	if r.Method != "POST" {
		http.Error(w, "", http.StatusMethodNotAllowed)
		return
	}

	//fetch url
	url, e := ioutil.ReadAll(r.Body)
	//"http://i.imgur.com/m1UIjW1.jpg"
	if e != nil {
		log.Println("Unable to load POST input data: actual error below..")
		log.Println(e)
		http.Error(w, "Invalid input POST data", http.StatusBadRequest)
		return
	}

	//log.Println("Entering cache")
	urlString := string(url)

	if strings.HasPrefix(urlString, "http") {
		//check if exists in cache
		if entry, err := cache.Get(hash(urlString)); err == nil {
			//log.Println("Cache hit")
			io.WriteString(w, string(entry))
			return
		}

		//log.Println("Passed cache")
		// connect to url
		response, e := http.Get(string(url))
		if e != nil {
			log.Println("Url unreachable or invalid, actual error below...")
			log.Println(e)
			http.Error(w, "Url unreachable or invalid", http.StatusBadRequest)
			return
		}
		// get image content from url
		buffer, e = ioutil.ReadAll(response.Body)
		defer response.Body.Close()
		if e != nil {
			log.Println("Url content generated error, actual error below...")
			log.Println(e)
			http.Error(w, "Content returned by url is invalid", http.StatusBadRequest)
			return
		}

	} else {
		//convert base64 encoded image to bytes
		img := base64.NewDecoder(base64.StdEncoding, strings.NewReader(string(url)))
		buf := new(bytes.Buffer)
		buf.ReadFrom(img)
		buffer = buf.Bytes()
	}

	//check if the input is actually an image
	imgtype := extractImgType(buffer)
	if imgtype == "gif" {
		r := bytes.NewReader(buffer)
		buf, e := getGifFrame(r)
		if e != nil {
			log.Println("GIF frame could not be extracted from image")
			log.Println(e)
			http.Error(w, "GIF frame could not be extracted from image", http.StatusBadRequest)
			return
		}
		//load the first frame
		buffer = buf.Bytes()

	}

	if imgtype == "nop" {
		log.Println("Image content is not a valid image")
		http.Error(w, "Image content is not a valid image", http.StatusBadRequest)
		return
	}

	//webp to png conversion
	if imgtype == "webp" {
		newImage, err := bimg.NewImage(buffer).Convert(bimg.PNG)
		if err != nil {
			log.Println("webP conversion error, actual error below")
			log.Println(err)
			http.Error(w, "Webp image conversion error", http.StatusBadRequest)
			return
		}

		buffer = newImage

	}

	//Run classifier
	cstr, e := C.classifier_classify(ctx, (*C.char)(unsafe.Pointer(&buffer[0])), C.size_t(len(buffer)))
	if e != nil {
		log.Println("Network prediction generated error or invalid image content, actual error below...")
		log.Println(e)
		http.Error(w, "Network predict error or invalid image content", http.StatusBadRequest)
		return
	}
	defer C.free(unsafe.Pointer(cstr))
	//add to cache
	cache.Set(hash(urlString), []byte(C.GoString(cstr)))
	//log to file
	end := time.Now()
	latency := end.Sub(start)
	if strings.HasPrefix(urlString, "http") {
	  logger.Printf("%v|%s|%s|%v",end.Format("2006/01/02 15:04:05"),urlString,C.GoString(cstr),latency)
	  }
	//return
	io.WriteString(w, C.GoString(cstr))
       */
       return
}

func init() {
	filepath := "/domains.dat"
	readDict(filepath)
}

//main thread sets up the API routes & starts the web server
